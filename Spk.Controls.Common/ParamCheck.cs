﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spk.Controls.Common
{
    public static class ParamCheck
    {
        public static void CheckNull(this object value, string name)
        {
            if (value == null)
                throw new ArgumentNullException(String.Format("Parameter {0} cannot be null!", name), name);
        }
    }
}
