﻿using System;
using Spk.Controls.Common;
using System.Drawing;

namespace Spk.Controls.Trees
{
    public class GetNodeHeightEventArgs : EventArgs
    {
        private VirtualNode node;
        private Graphics graphics;
        private uint height;

        public GetNodeHeightEventArgs(VirtualNode newNode, Graphics newGraphics, uint currentHeight)
        {
            newNode.CheckNull("node");
            newGraphics.CheckNull("newGraphics");
            node = newNode;
            graphics = newGraphics;
            height = currentHeight;
        }

        public VirtualNode Node
        {
            get
            {
                return node;
            }
        }

        public Graphics Graphics
        {
            get
            {
                return graphics;
            }
        }

        public uint Height
        {
            get
            {
                return height;
            }
            set
            {
                height = value;
            }
        }
    }

    public delegate void GetNodeHeightEventHandler(object sender, GetNodeHeightEventArgs e);

    public class GetNodeWidthEventArgs : EventArgs
    {
        private VirtualNode node;
        private Graphics graphics;
        private uint width;

        public GetNodeWidthEventArgs(VirtualNode newNode, Graphics newGraphics, uint currentWidth)
        {
            newNode.CheckNull("node");
            newGraphics.CheckNull("newGraphics");
            node = newNode;
            graphics = newGraphics;
            width = currentWidth;
        }

        public VirtualNode Node
        {
            get
            {
                return node;
            }
        }

        public Graphics Graphics
        {
            get
            {
                return graphics;
            }
        }

        public uint Width
        {
            get
            {
                return width;
            }
            set
            {
                width = value;
            }
        }
    }

    public delegate void GetNodeWidthEventHandler(object sender, GetNodeWidthEventArgs e);

    public class GetNodeContentsWidthEventArgs : EventArgs
    {
        private VirtualNode node;
        private Graphics graphics;
        private uint width;

        public GetNodeContentsWidthEventArgs(VirtualNode newNode, Graphics newGraphics, uint currentWidth)
        {
            newNode.CheckNull("node");
            newGraphics.CheckNull("newGraphics");
            node = newNode;
            graphics = newGraphics;
            width = currentWidth;
        }

        public VirtualNode Node
        {
            get
            {
                return node;
            }
        }

        public Graphics Graphics
        {
            get
            {
                return graphics;
            }
        }

        public uint Width
        {
            get
            {
                return width;
            }
            set
            {
                width = value;
            }
        }
    }

    public delegate void GetNodeContentsWidthEventHandler(object sender, GetNodeContentsWidthEventArgs e);

    public class GetChildCountEventArgs : EventArgs
    {
        private VirtualNode node;
        private uint childCount;

        public GetChildCountEventArgs(VirtualNode newNode)
        {
            newNode.CheckNull("node");
            node = newNode;
            childCount = 0;
        }

        public VirtualNode Node
        {
            get
            {
                return node;
            }
        }

        public uint ChildCount
        {
            get
            {
                return childCount;
            }
            set
            {
                childCount = value;
            }
        }
    }

    public delegate void GetChildCountHandler(object sender, GetChildCountEventArgs e);

    public class GetTextEventArgs : EventArgs
    {
        private VirtualNode node;
        private string text;

        public GetTextEventArgs(VirtualNode newNode)
        {
            newNode.CheckNull("newNode");
            node = newNode;
        }

        public VirtualNode Node
        {
            get
            {
                return node;
            }
        }

        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
            }
        }
    }

    public delegate void GetTextHandler(object sender, GetTextEventArgs e);

    public class InitNodeEventArgs : EventArgs
    {
        private VirtualNode node;
        private bool visible;
        private bool @checked;
        private bool hasChildren;
        private object data;

        public InitNodeEventArgs(VirtualNode newNode)
        {
            newNode.CheckNull("newNode");

            node = newNode;
            visible = true;
            @checked = false;
            hasChildren = false;
            data = null;
        }

        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
            }
        }

        public bool Checked
        {
            get
            {
                return @checked;
            }
            set
            {
                @checked = value;
            }
        }

        public bool HasChildren
        {
            get
            {
                return hasChildren;
            }
            set
            {
                hasChildren = value;
            }
        }

        public object Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }

        public VirtualNode Node
        {
            get
            {
                return node;
            }
        }
    }

    public delegate void InitNodeEventHandler(object sender, InitNodeEventArgs e);

    public class GetImageIndexEventArgs : EventArgs
    {
        private VirtualNode node;
        int imageIndex;

        public GetImageIndexEventArgs(VirtualNode newNode)
        {
            newNode.CheckNull("newNode");

            node = newNode;
            imageIndex = -1;
        }

        public VirtualNode Node
        {
            get
            {
                return node;
            }
        }

        public int ImageIndex
        {
            get
            {
                return imageIndex;
            }
            set
            {
                imageIndex = value;
            }
        }
    }

    public delegate void GetImageIndexHandler(object sender, GetImageIndexEventArgs e);

    public class DrawNodeContentsEventArgs : EventArgs
    {
        private VirtualNode node;
        private Rectangle contentsRect;
        private Graphics graphics;
        private bool forceDrawSelection;

        public DrawNodeContentsEventArgs(VirtualNode newNode, Rectangle newContentsRectangle, Graphics newGraphics, bool newForceDrawSelection)
        {
            newNode.CheckNull("newNode");
            newContentsRectangle.CheckNull("newContentsRectangle");
            newGraphics.CheckNull("newGraphics");

            node = newNode;
            contentsRect = newContentsRectangle;
            graphics = newGraphics;
            forceDrawSelection = newForceDrawSelection;
        }

        public VirtualNode Node
        {
            get
            {
                return node;
            }
        }

        public Rectangle ContentsRect
        {
            get
            {
                return contentsRect;
            }
        }

        public Graphics Graphics
        {
            get
            {
                return graphics;
            }
        }

        public bool ForceDrawSelection
        {
            get
            {
                return forceDrawSelection;
            }
        }
    }

    public delegate void DrawNodeContentsHandler(object sender, DrawNodeContentsEventArgs e);

    public class DrawNodeEventArgs : EventArgs
    {
        private VirtualNode node;
        private Rectangle nodeRect;
        private Graphics graphics;
        private bool forceDrawSelection;

        public DrawNodeEventArgs(VirtualNode newNode,
            Rectangle newNodeRectangle,
            Graphics newGraphics,
            bool newForceDrawSelection)
        {
            newNode.CheckNull("newNode");
            newNodeRectangle.CheckNull("newContentsRectangle");
            newGraphics.CheckNull("newGraphics");

            node = newNode;
            nodeRect = newNodeRectangle;
            graphics = newGraphics;
            forceDrawSelection = newForceDrawSelection;
        }

        public VirtualNode Node
        {
            get
            {
                return node;
            }
        }

        public Rectangle NodeRect
        {
            get
            {
                return nodeRect;
            }
        }

        public Graphics Graphics
        {
            get
            {
                return graphics;
            }
        }

        public bool ForceDrawSelection
        {
            get
            {
                return forceDrawSelection;
            }
        }
    }

    public delegate void DrawNodeHandler(object sender, DrawNodeEventArgs e);

    public class NodeEventArgs : EventArgs
    {
        private VirtualNode node;

        public NodeEventArgs(VirtualNode newNode)
        {
            newNode.CheckNull("newNode");

            node = newNode;
        }

        public VirtualNode Node
        {
            get
            {
                return node;
            }
        }
    }

    public delegate void NodeEventHandler(object sender, NodeEventArgs e);

    public class GetNodeRegionsEventArgs : EventArgs
    {
        private VirtualNode node;
        private Rectangle nodeRect;
        private Rectangle expandMarkRect;
        private Rectangle contentsRect;
        private Rectangle checkboxRect;

        public GetNodeRegionsEventArgs(VirtualNode newNode,
            Rectangle newNodeRect)
        {
            node = newNode;
            nodeRect = newNodeRect;
            expandMarkRect = new Rectangle(nodeRect.Left, nodeRect.Top, 0, 0);
            contentsRect = nodeRect;
        }

        public VirtualNode Node
        {
            get
            {
                return node;
            }
        }

        public Rectangle NodeRectangle
        {
            get
            {
                return nodeRect;
            }
        }

        public Rectangle ExpandMarkRectangle
        {
            get
            {
                return expandMarkRect;
            }
            set
            {
                if (!Rectangle.Equals(Rectangle.Intersect(nodeRect, value), value))
                    throw new ArgumentException("Expand mark rectangle must be within the node rectangle!");

                expandMarkRect = value;
            }
        }

        public Rectangle CheckboxRectangle
        {
            get
            {
                return checkboxRect;
            }
            set
            {
                if (!Rectangle.Equals(Rectangle.Intersect(nodeRect, value), value))
                    throw new ArgumentException("Expand mark rectangle must be within the node rectangle!");

                checkboxRect = value;
            }
        }

        public Rectangle ContentsRectangle
        {
            get
            {
                return contentsRect;
            }
            set
            {
                if (!Rectangle.Equals(Rectangle.Intersect(nodeRect, value), value))
                    throw new ArgumentException("Expand mark rectangle must be within the node rectangle!");

                contentsRect = value;
            }
        }
    }

    public delegate void GetNodeRegionsHandler(object sender, GetNodeRegionsEventArgs e);

    public class GetNodeCheckboxVisibilityEventArgs : EventArgs
    {
        private VirtualNode node;
        private bool showCheckbox;

        public GetNodeCheckboxVisibilityEventArgs(VirtualNode newNode)
        {
            newNode.CheckNull("newNode");
            node = newNode;
            showCheckbox = true;
        }

        public VirtualNode Node
        {
            get
            {
                return node;
            }
        }

        public bool ShowCheckbox
        {
            get
            {
                return showCheckbox;
            }
            set
            {
                showCheckbox = value;
            }
        }
    }

    public delegate void GetNodeCheckboxVisibilityEventHandler(object sender, GetNodeCheckboxVisibilityEventArgs e);
}