﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spk.Controls.Trees
{
    public enum DrawModes
    {
        Default,
        OwnerDrawItemContents,
        OwnerDrawItems,
        System
    }
}
