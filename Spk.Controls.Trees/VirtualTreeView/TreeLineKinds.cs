﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spk.Controls.Trees
{
    public enum TreeLineKinds
    {
        OnlyRoot,   // ─
        FirstRoot,  // ┌
        OnlyInner,  // └ with margin at top
        FirstInner, // ├ with margin at top
        Middle,     // ├
        Last,       // └
        Line,       // │
        None        // 
    }
}
