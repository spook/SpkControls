﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Spk.Controls.Trees
{
    /// <summary>
    /// Scrollbar used inside the treeview control.
    /// It differs from original scrollbar only by lack of ability
    /// to receive focus.
    /// </summary>
    internal class VTVHorizontalScrollBar : HScrollBar
    {
        public VTVHorizontalScrollBar()
        {
            SetStyle(ControlStyles.Selectable, false);
        }
    }
}
