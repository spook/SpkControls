﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spk.Controls.Trees
{
    [Serializable]
    public class VirtualTreeViewException : Exception
    {
        public VirtualTreeViewException() 
        {

        }

        public VirtualTreeViewException(string message) : base(message) 
        {

        }

        public VirtualTreeViewException(string message, Exception inner) : base(message, inner) 
        {

        }

        protected VirtualTreeViewException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) 
        {

        }
    }
}
