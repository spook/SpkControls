﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Spk.Controls.Demo
{
    /// <summary>
    /// TabControl catches Ctrl+PageUp/PageDown shortcuts on
    /// their way to VirtualTreeView, thus unabling to test
    /// them on the tree control. This class is a hack to
    /// disable them for testing purposes.
    /// </summary>
    class KeylessTabControl : TabControl
    {
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == (Keys.PageUp | Keys.Control) ||
                e.KeyData == (Keys.PageDown | Keys.Control))
                return;

            base.OnKeyDown(e);
        }
    }
}
