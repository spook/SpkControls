﻿namespace Spk.Controls.Demo
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.tabControl1 = new Spk.Controls.Demo.KeylessTabControl();
            this.tpVTV = new System.Windows.Forms.TabPage();
            this.tabControl2 = new Spk.Controls.Demo.KeylessTabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.vtvTree = new Spk.Controls.Trees.VirtualTreeView();
            this.ilImages = new System.Windows.Forms.ImageList(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.lbCheckedNodes = new System.Windows.Forms.ListBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.lbSelection = new System.Windows.Forms.ListBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.bSetNodeHeight = new System.Windows.Forms.Button();
            this.tbNodeHeight = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.bSetFontSize = new System.Windows.Forms.Button();
            this.tbFontSize = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbDrawMode = new System.Windows.Forms.ComboBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.bShowChosen = new System.Windows.Forms.Button();
            this.lbHiddenNodes = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.bHideSelected = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.bInsertAfter = new System.Windows.Forms.Button();
            this.bInsertBefore = new System.Windows.Forms.Button();
            this.bScrollToFocused = new System.Windows.Forms.Button();
            this.bDeleteChildNodes = new System.Windows.Forms.Button();
            this.bSetChildCount = new System.Windows.Forms.Button();
            this.bDeleteNode = new System.Windows.Forms.Button();
            this.bToggleSelected = new System.Windows.Forms.Button();
            this.tbChildCount = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cbHotTrack = new System.Windows.Forms.CheckBox();
            this.cbAllowExpandCollapse = new System.Windows.Forms.CheckBox();
            this.cbShowCheckboxes = new System.Windows.Forms.CheckBox();
            this.cbShowImages = new System.Windows.Forms.CheckBox();
            this.cbShowExpandMarks = new System.Windows.Forms.CheckBox();
            this.cbShowRootLines = new System.Windows.Forms.CheckBox();
            this.cbShowLines = new System.Windows.Forms.CheckBox();
            this.cbMultiselect = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.bSetRootNodeCount = new System.Windows.Forms.Button();
            this.tbRootNodeCount = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.bAddChild = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tpVTV.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl1.Controls.Add(this.tpVTV);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1153, 871);
            this.tabControl1.TabIndex = 0;
            // 
            // tpVTV
            // 
            this.tpVTV.Controls.Add(this.tabControl2);
            this.tpVTV.Location = new System.Drawing.Point(4, 25);
            this.tpVTV.Margin = new System.Windows.Forms.Padding(4);
            this.tpVTV.Name = "tpVTV";
            this.tpVTV.Padding = new System.Windows.Forms.Padding(4);
            this.tpVTV.Size = new System.Drawing.Size(1145, 842);
            this.tpVTV.TabIndex = 0;
            this.tpVTV.Text = "VirtualTreeView";
            this.tpVTV.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Location = new System.Drawing.Point(4, 4);
            this.tabControl2.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(1135, 832);
            this.tabControl2.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(1127, 803);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Simple example";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.vtvTree);
            this.groupBox1.Location = new System.Drawing.Point(8, 7);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(389, 782);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " VirtualTreeView preview ";
            // 
            // vtvTree
            // 
            this.vtvTree.AllowExpandCollapse = true;
            this.vtvTree.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.vtvTree.DrawMode = Spk.Controls.Trees.DrawModes.Default;
            this.vtvTree.HotTrack = false;
            this.vtvTree.Images = this.ilImages;
            this.vtvTree.Location = new System.Drawing.Point(8, 23);
            this.vtvTree.Margin = new System.Windows.Forms.Padding(4);
            this.vtvTree.MultiSelect = false;
            this.vtvTree.Name = "vtvTree";
            this.vtvTree.ShowBorder = true;
            this.vtvTree.ShowCheckboxes = false;
            this.vtvTree.ShowExpandMarks = true;
            this.vtvTree.ShowImages = true;
            this.vtvTree.ShowLines = true;
            this.vtvTree.ShowRootLines = true;
            this.vtvTree.Size = new System.Drawing.Size(373, 716);
            this.vtvTree.TabIndex = 0;
            this.vtvTree.InitNode += new Spk.Controls.Trees.InitNodeEventHandler(this.vtvTree_GetNodeInitialData);
            this.vtvTree.GetChildCount += new Spk.Controls.Trees.GetChildCountHandler(this.vtvTree_GetChildCount);
            this.vtvTree.GetText += new Spk.Controls.Trees.GetTextHandler(this.vtvTree_GetText);
            this.vtvTree.GetImageIndex += new Spk.Controls.Trees.GetImageIndexHandler(this.vtvTree_GetImageIndex);
            this.vtvTree.GetNodeHeight += new Spk.Controls.Trees.GetNodeHeightEventHandler(this.vtvTree_GetNodeHeight);
            this.vtvTree.GetNodeWidth += new Spk.Controls.Trees.GetNodeWidthEventHandler(this.vtvTree_GetNodeWidth);
            this.vtvTree.GetNodeContentsWidth += new Spk.Controls.Trees.GetNodeContentsWidthEventHandler(this.vtvTree_GetNodeContentsWidth);
            this.vtvTree.DrawNodeContents += new Spk.Controls.Trees.DrawNodeContentsHandler(this.vtvTree_DrawNodeContents);
            this.vtvTree.DrawNode += new Spk.Controls.Trees.DrawNodeHandler(this.vtvTree_DrawNode);
            this.vtvTree.SelectionChanged += new System.EventHandler(this.vtvTree_SelectionChanged);
            this.vtvTree.CheckedNodesChanged += new System.EventHandler(this.vtvTree_CheckedNodesChanged);
            // 
            // ilImages
            // 
            this.ilImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilImages.ImageStream")));
            this.ilImages.TransparentColor = System.Drawing.Color.Transparent;
            this.ilImages.Images.SetKeyName(0, "arrow_left_16_h.png");
            this.ilImages.Images.SetKeyName(1, "arrow_right_16_h.png");
            this.ilImages.Images.SetKeyName(2, "copy_clipboard_16_h.png");
            this.ilImages.Images.SetKeyName(3, "copy_clipboard_lined_16_h.png");
            this.ilImages.Images.SetKeyName(4, "copy_to_folder_16_h.png");
            this.ilImages.Images.SetKeyName(5, "cut_clipboard_16_h.png");
            this.ilImages.Images.SetKeyName(6, "delete_16_h.png");
            this.ilImages.Images.SetKeyName(7, "favorites_16_h.png");
            this.ilImages.Images.SetKeyName(8, "folder_closed_16_h.png");
            this.ilImages.Images.SetKeyName(9, "folder_open_16_h.png");
            this.ilImages.Images.SetKeyName(10, "folder_options_16_h.png");
            this.ilImages.Images.SetKeyName(11, "folder_options_b_16_h.png");
            this.ilImages.Images.SetKeyName(12, "folders_16_h.png");
            this.ilImages.Images.SetKeyName(13, "history_1_16_h.png");
            this.ilImages.Images.SetKeyName(14, "history_16_h.png");
            this.ilImages.Images.SetKeyName(15, "history_b_16_h.png");
            this.ilImages.Images.SetKeyName(16, "history_b1_16_h.png");
            this.ilImages.Images.SetKeyName(17, "home_16_h.png");
            this.ilImages.Images.SetKeyName(18, "mail_16_h.png");
            this.ilImages.Images.SetKeyName(19, "mail_b_16_h.png");
            this.ilImages.Images.SetKeyName(20, "move_to_folder_16_h.png");
            this.ilImages.Images.SetKeyName(21, "new_document_16_h.png");
            this.ilImages.Images.SetKeyName(22, "new_document_lined_16_h.png");
            this.ilImages.Images.SetKeyName(23, "open_document_16_h.png");
            this.ilImages.Images.SetKeyName(24, "paste_clipboard_16_h.png");
            this.ilImages.Images.SetKeyName(25, "paste_clipboard_lined_16_h.png");
            this.ilImages.Images.SetKeyName(26, "print_16_h.png");
            this.ilImages.Images.SetKeyName(27, "print_preview_16_h.png");
            this.ilImages.Images.SetKeyName(28, "print_preview_lined_16_h.png");
            this.ilImages.Images.SetKeyName(29, "properties_document_16_h.png");
            this.ilImages.Images.SetKeyName(30, "properties_document_b_16_h.png");
            this.ilImages.Images.SetKeyName(31, "properties_document_b_lined_16_h.png");
            this.ilImages.Images.SetKeyName(32, "properties_document_lined_16_h.png");
            this.ilImages.Images.SetKeyName(33, "redo_16_h.png");
            this.ilImages.Images.SetKeyName(34, "redo_square_16_h.png");
            this.ilImages.Images.SetKeyName(35, "refresh_document_16_h.png");
            this.ilImages.Images.SetKeyName(36, "save_16_h.png");
            this.ilImages.Images.SetKeyName(37, "search_16_h.png");
            this.ilImages.Images.SetKeyName(38, "stop_16_h.png");
            this.ilImages.Images.SetKeyName(39, "undo_16_h.png");
            this.ilImages.Images.SetKeyName(40, "undo_square_16_h.png");
            this.ilImages.Images.SetKeyName(41, "up_folder_16_h.png");
            this.ilImages.Images.SetKeyName(42, "view_16_h.png");
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.groupBox9);
            this.groupBox2.Controls.Add(this.groupBox8);
            this.groupBox2.Controls.Add(this.groupBox7);
            this.groupBox2.Controls.Add(this.groupBox6);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Location = new System.Drawing.Point(405, 7);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(711, 782);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " Control panel ";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.lbCheckedNodes);
            this.groupBox9.Location = new System.Drawing.Point(400, 586);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox9.Size = new System.Drawing.Size(292, 187);
            this.groupBox9.TabIndex = 6;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = " Checked nodes ";
            // 
            // lbCheckedNodes
            // 
            this.lbCheckedNodes.FormattingEnabled = true;
            this.lbCheckedNodes.ItemHeight = 16;
            this.lbCheckedNodes.Location = new System.Drawing.Point(8, 23);
            this.lbCheckedNodes.Margin = new System.Windows.Forms.Padding(4);
            this.lbCheckedNodes.Name = "lbCheckedNodes";
            this.lbCheckedNodes.Size = new System.Drawing.Size(273, 148);
            this.lbCheckedNodes.TabIndex = 0;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.lbSelection);
            this.groupBox8.Location = new System.Drawing.Point(400, 352);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox8.Size = new System.Drawing.Size(292, 226);
            this.groupBox8.TabIndex = 5;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = " Current selection ";
            // 
            // lbSelection
            // 
            this.lbSelection.FormattingEnabled = true;
            this.lbSelection.ItemHeight = 16;
            this.lbSelection.Location = new System.Drawing.Point(8, 27);
            this.lbSelection.Margin = new System.Windows.Forms.Padding(4);
            this.lbSelection.Name = "lbSelection";
            this.lbSelection.Size = new System.Drawing.Size(273, 180);
            this.lbSelection.TabIndex = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.bSetNodeHeight);
            this.groupBox7.Controls.Add(this.tbNodeHeight);
            this.groupBox7.Controls.Add(this.label6);
            this.groupBox7.Controls.Add(this.bSetFontSize);
            this.groupBox7.Controls.Add(this.tbFontSize);
            this.groupBox7.Controls.Add(this.label5);
            this.groupBox7.Controls.Add(this.label4);
            this.groupBox7.Controls.Add(this.cbDrawMode);
            this.groupBox7.Location = new System.Drawing.Point(8, 641);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox7.Size = new System.Drawing.Size(384, 132);
            this.groupBox7.TabIndex = 4;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = " Appearance ";
            // 
            // bSetNodeHeight
            // 
            this.bSetNodeHeight.Enabled = false;
            this.bSetNodeHeight.Location = new System.Drawing.Point(305, 92);
            this.bSetNodeHeight.Margin = new System.Windows.Forms.Padding(4);
            this.bSetNodeHeight.Name = "bSetNodeHeight";
            this.bSetNodeHeight.Size = new System.Drawing.Size(71, 28);
            this.bSetNodeHeight.TabIndex = 30;
            this.bSetNodeHeight.Text = "Set";
            this.bSetNodeHeight.UseVisualStyleBackColor = true;
            this.bSetNodeHeight.Click += new System.EventHandler(this.bSetNodeHeight_Click);
            // 
            // tbNodeHeight
            // 
            this.tbNodeHeight.Enabled = false;
            this.tbNodeHeight.Location = new System.Drawing.Point(132, 95);
            this.tbNodeHeight.Margin = new System.Windows.Forms.Padding(4);
            this.tbNodeHeight.Name = "tbNodeHeight";
            this.tbNodeHeight.Size = new System.Drawing.Size(164, 22);
            this.tbNodeHeight.TabIndex = 29;
            this.tbNodeHeight.Text = "100";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 98);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 17);
            this.label6.TabIndex = 28;
            this.label6.Text = "Node height";
            // 
            // bSetFontSize
            // 
            this.bSetFontSize.Location = new System.Drawing.Point(305, 57);
            this.bSetFontSize.Margin = new System.Windows.Forms.Padding(4);
            this.bSetFontSize.Name = "bSetFontSize";
            this.bSetFontSize.Size = new System.Drawing.Size(71, 28);
            this.bSetFontSize.TabIndex = 27;
            this.bSetFontSize.Text = "Set";
            this.bSetFontSize.UseVisualStyleBackColor = true;
            this.bSetFontSize.Click += new System.EventHandler(this.bSetFontSize_Click);
            // 
            // tbFontSize
            // 
            this.tbFontSize.Location = new System.Drawing.Point(132, 59);
            this.tbFontSize.Margin = new System.Windows.Forms.Padding(4);
            this.tbFontSize.Name = "tbFontSize";
            this.tbFontSize.Size = new System.Drawing.Size(164, 22);
            this.tbFontSize.TabIndex = 26;
            this.tbFontSize.Text = "12";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 63);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 17);
            this.label5.TabIndex = 25;
            this.label5.Text = "Font size";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 27);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 17);
            this.label4.TabIndex = 24;
            this.label4.Text = "Draw mode";
            // 
            // cbDrawMode
            // 
            this.cbDrawMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbDrawMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDrawMode.FormattingEnabled = true;
            this.cbDrawMode.Items.AddRange(new object[] {
            "Default",
            "OwnerDrawNodeContents",
            "OwnerDrawNode",
            "System"});
            this.cbDrawMode.Location = new System.Drawing.Point(132, 23);
            this.cbDrawMode.Margin = new System.Windows.Forms.Padding(4);
            this.cbDrawMode.Name = "cbDrawMode";
            this.cbDrawMode.Size = new System.Drawing.Size(243, 24);
            this.cbDrawMode.TabIndex = 23;
            this.cbDrawMode.SelectedIndexChanged += new System.EventHandler(this.cbDrawMode_SelectedIndexChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.bShowChosen);
            this.groupBox6.Controls.Add(this.lbHiddenNodes);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.bHideSelected);
            this.groupBox6.Location = new System.Drawing.Point(400, 23);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox6.Size = new System.Drawing.Size(292, 321);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = " Show / hide ";
            // 
            // bShowChosen
            // 
            this.bShowChosen.Location = new System.Drawing.Point(8, 286);
            this.bShowChosen.Margin = new System.Windows.Forms.Padding(4);
            this.bShowChosen.Name = "bShowChosen";
            this.bShowChosen.Size = new System.Drawing.Size(275, 28);
            this.bShowChosen.TabIndex = 3;
            this.bShowChosen.Text = "Show chosen";
            this.bShowChosen.UseVisualStyleBackColor = true;
            this.bShowChosen.Click += new System.EventHandler(this.bShowChosen_Click);
            // 
            // lbHiddenNodes
            // 
            this.lbHiddenNodes.FormattingEnabled = true;
            this.lbHiddenNodes.IntegralHeight = false;
            this.lbHiddenNodes.ItemHeight = 16;
            this.lbHiddenNodes.Location = new System.Drawing.Point(8, 73);
            this.lbHiddenNodes.Margin = new System.Windows.Forms.Padding(4);
            this.lbHiddenNodes.Name = "lbHiddenNodes";
            this.lbHiddenNodes.Size = new System.Drawing.Size(273, 205);
            this.lbHiddenNodes.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 53);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Hidden node list";
            // 
            // bHideSelected
            // 
            this.bHideSelected.Location = new System.Drawing.Point(8, 21);
            this.bHideSelected.Margin = new System.Windows.Forms.Padding(4);
            this.bHideSelected.Name = "bHideSelected";
            this.bHideSelected.Size = new System.Drawing.Size(275, 28);
            this.bHideSelected.TabIndex = 0;
            this.bHideSelected.Text = "Hide selected";
            this.bHideSelected.UseVisualStyleBackColor = true;
            this.bHideSelected.Click += new System.EventHandler(this.bHideSelected_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.bAddChild);
            this.groupBox5.Controls.Add(this.bInsertAfter);
            this.groupBox5.Controls.Add(this.bInsertBefore);
            this.groupBox5.Controls.Add(this.bScrollToFocused);
            this.groupBox5.Controls.Add(this.bDeleteChildNodes);
            this.groupBox5.Controls.Add(this.bSetChildCount);
            this.groupBox5.Controls.Add(this.bDeleteNode);
            this.groupBox5.Controls.Add(this.bToggleSelected);
            this.groupBox5.Controls.Add(this.tbChildCount);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Location = new System.Drawing.Point(8, 158);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(384, 203);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = " Control ";
            // 
            // bInsertAfter
            // 
            this.bInsertAfter.Location = new System.Drawing.Point(202, 124);
            this.bInsertAfter.Name = "bInsertAfter";
            this.bInsertAfter.Size = new System.Drawing.Size(173, 27);
            this.bInsertAfter.TabIndex = 9;
            this.bInsertAfter.Text = "Insert after selected";
            this.bInsertAfter.UseVisualStyleBackColor = true;
            this.bInsertAfter.Click += new System.EventHandler(this.bInsertAfter_Click);
            // 
            // bInsertBefore
            // 
            this.bInsertBefore.Location = new System.Drawing.Point(202, 90);
            this.bInsertBefore.Name = "bInsertBefore";
            this.bInsertBefore.Size = new System.Drawing.Size(173, 27);
            this.bInsertBefore.TabIndex = 8;
            this.bInsertBefore.Text = "Insert before selected";
            this.bInsertBefore.UseVisualStyleBackColor = true;
            this.bInsertBefore.Click += new System.EventHandler(this.bInsertBefore_Click);
            // 
            // bScrollToFocused
            // 
            this.bScrollToFocused.Location = new System.Drawing.Point(12, 159);
            this.bScrollToFocused.Margin = new System.Windows.Forms.Padding(4);
            this.bScrollToFocused.Name = "bScrollToFocused";
            this.bScrollToFocused.Size = new System.Drawing.Size(183, 28);
            this.bScrollToFocused.TabIndex = 7;
            this.bScrollToFocused.Text = "Scroll to focused";
            this.bScrollToFocused.UseVisualStyleBackColor = true;
            this.bScrollToFocused.Click += new System.EventHandler(this.bScrollToFocused_Click);
            // 
            // bDeleteChildNodes
            // 
            this.bDeleteChildNodes.Location = new System.Drawing.Point(12, 124);
            this.bDeleteChildNodes.Margin = new System.Windows.Forms.Padding(4);
            this.bDeleteChildNodes.Name = "bDeleteChildNodes";
            this.bDeleteChildNodes.Size = new System.Drawing.Size(183, 27);
            this.bDeleteChildNodes.TabIndex = 6;
            this.bDeleteChildNodes.Text = "Delete child nodes";
            this.bDeleteChildNodes.UseVisualStyleBackColor = true;
            this.bDeleteChildNodes.Click += new System.EventHandler(this.bDeleteChildNodes_Click);
            // 
            // bSetChildCount
            // 
            this.bSetChildCount.Location = new System.Drawing.Point(305, 55);
            this.bSetChildCount.Margin = new System.Windows.Forms.Padding(4);
            this.bSetChildCount.Name = "bSetChildCount";
            this.bSetChildCount.Size = new System.Drawing.Size(71, 28);
            this.bSetChildCount.TabIndex = 5;
            this.bSetChildCount.Text = "Set";
            this.bSetChildCount.UseVisualStyleBackColor = true;
            this.bSetChildCount.Click += new System.EventHandler(this.bSetChildCount_Click);
            // 
            // bDeleteNode
            // 
            this.bDeleteNode.Location = new System.Drawing.Point(12, 90);
            this.bDeleteNode.Margin = new System.Windows.Forms.Padding(4);
            this.bDeleteNode.Name = "bDeleteNode";
            this.bDeleteNode.Size = new System.Drawing.Size(183, 27);
            this.bDeleteNode.TabIndex = 4;
            this.bDeleteNode.Text = "Delete node";
            this.bDeleteNode.UseVisualStyleBackColor = true;
            this.bDeleteNode.Click += new System.EventHandler(this.bDeleteNode_Click);
            // 
            // bToggleSelected
            // 
            this.bToggleSelected.Location = new System.Drawing.Point(12, 23);
            this.bToggleSelected.Margin = new System.Windows.Forms.Padding(4);
            this.bToggleSelected.Name = "bToggleSelected";
            this.bToggleSelected.Size = new System.Drawing.Size(364, 27);
            this.bToggleSelected.TabIndex = 3;
            this.bToggleSelected.Text = "Toggle selected";
            this.bToggleSelected.UseVisualStyleBackColor = true;
            this.bToggleSelected.Click += new System.EventHandler(this.bToggleSelected_Click);
            // 
            // tbChildCount
            // 
            this.tbChildCount.Location = new System.Drawing.Point(132, 58);
            this.tbChildCount.Margin = new System.Windows.Forms.Padding(4);
            this.tbChildCount.Name = "tbChildCount";
            this.tbChildCount.Size = new System.Drawing.Size(164, 22);
            this.tbChildCount.TabIndex = 2;
            this.tbChildCount.Text = "5";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 62);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Child count";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cbHotTrack);
            this.groupBox4.Controls.Add(this.cbAllowExpandCollapse);
            this.groupBox4.Controls.Add(this.cbShowCheckboxes);
            this.groupBox4.Controls.Add(this.cbShowImages);
            this.groupBox4.Controls.Add(this.cbShowExpandMarks);
            this.groupBox4.Controls.Add(this.cbShowRootLines);
            this.groupBox4.Controls.Add(this.cbShowLines);
            this.groupBox4.Controls.Add(this.cbMultiselect);
            this.groupBox4.Location = new System.Drawing.Point(8, 368);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(384, 266);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = " Properties ";
            // 
            // cbHotTrack
            // 
            this.cbHotTrack.AutoSize = true;
            this.cbHotTrack.Location = new System.Drawing.Point(12, 52);
            this.cbHotTrack.Margin = new System.Windows.Forms.Padding(4);
            this.cbHotTrack.Name = "cbHotTrack";
            this.cbHotTrack.Size = new System.Drawing.Size(83, 21);
            this.cbHotTrack.TabIndex = 7;
            this.cbHotTrack.Text = "Hottrack";
            this.cbHotTrack.UseVisualStyleBackColor = true;
            this.cbHotTrack.CheckedChanged += new System.EventHandler(this.cbHotTrack_CheckedChanged);
            // 
            // cbAllowExpandCollapse
            // 
            this.cbAllowExpandCollapse.AutoSize = true;
            this.cbAllowExpandCollapse.Checked = true;
            this.cbAllowExpandCollapse.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAllowExpandCollapse.Location = new System.Drawing.Point(12, 222);
            this.cbAllowExpandCollapse.Margin = new System.Windows.Forms.Padding(4);
            this.cbAllowExpandCollapse.Name = "cbAllowExpandCollapse";
            this.cbAllowExpandCollapse.Size = new System.Drawing.Size(168, 21);
            this.cbAllowExpandCollapse.TabIndex = 6;
            this.cbAllowExpandCollapse.Text = "Allow expand/collapse";
            this.cbAllowExpandCollapse.UseVisualStyleBackColor = true;
            this.cbAllowExpandCollapse.CheckedChanged += new System.EventHandler(this.cbAllowExpandCollapse_CheckedChanged);
            // 
            // cbShowCheckboxes
            // 
            this.cbShowCheckboxes.AutoSize = true;
            this.cbShowCheckboxes.Location = new System.Drawing.Point(12, 193);
            this.cbShowCheckboxes.Margin = new System.Windows.Forms.Padding(4);
            this.cbShowCheckboxes.Name = "cbShowCheckboxes";
            this.cbShowCheckboxes.Size = new System.Drawing.Size(142, 21);
            this.cbShowCheckboxes.TabIndex = 5;
            this.cbShowCheckboxes.Text = "Show checkboxes";
            this.cbShowCheckboxes.UseVisualStyleBackColor = true;
            this.cbShowCheckboxes.CheckedChanged += new System.EventHandler(this.cbShowCheckboxes_CheckedChanged);
            // 
            // cbShowImages
            // 
            this.cbShowImages.AutoSize = true;
            this.cbShowImages.Checked = true;
            this.cbShowImages.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowImages.Location = new System.Drawing.Point(12, 165);
            this.cbShowImages.Margin = new System.Windows.Forms.Padding(4);
            this.cbShowImages.Name = "cbShowImages";
            this.cbShowImages.Size = new System.Drawing.Size(113, 21);
            this.cbShowImages.TabIndex = 4;
            this.cbShowImages.Text = "Show images";
            this.cbShowImages.UseVisualStyleBackColor = true;
            this.cbShowImages.CheckedChanged += new System.EventHandler(this.cbShowImages_CheckedChanged);
            // 
            // cbShowExpandMarks
            // 
            this.cbShowExpandMarks.AutoSize = true;
            this.cbShowExpandMarks.Checked = true;
            this.cbShowExpandMarks.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowExpandMarks.Location = new System.Drawing.Point(12, 137);
            this.cbShowExpandMarks.Margin = new System.Windows.Forms.Padding(4);
            this.cbShowExpandMarks.Name = "cbShowExpandMarks";
            this.cbShowExpandMarks.Size = new System.Drawing.Size(156, 21);
            this.cbShowExpandMarks.TabIndex = 3;
            this.cbShowExpandMarks.Text = "Show expand marks";
            this.cbShowExpandMarks.UseVisualStyleBackColor = true;
            this.cbShowExpandMarks.CheckedChanged += new System.EventHandler(this.cbShowExpandMarks_CheckedChanged);
            // 
            // cbShowRootLines
            // 
            this.cbShowRootLines.AutoSize = true;
            this.cbShowRootLines.Checked = true;
            this.cbShowRootLines.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowRootLines.Location = new System.Drawing.Point(12, 108);
            this.cbShowRootLines.Margin = new System.Windows.Forms.Padding(4);
            this.cbShowRootLines.Name = "cbShowRootLines";
            this.cbShowRootLines.Size = new System.Drawing.Size(126, 21);
            this.cbShowRootLines.TabIndex = 2;
            this.cbShowRootLines.Text = "Show root lines";
            this.cbShowRootLines.UseVisualStyleBackColor = true;
            this.cbShowRootLines.CheckedChanged += new System.EventHandler(this.cbShowRootLines_CheckedChanged);
            // 
            // cbShowLines
            // 
            this.cbShowLines.AutoSize = true;
            this.cbShowLines.Checked = true;
            this.cbShowLines.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowLines.Location = new System.Drawing.Point(12, 80);
            this.cbShowLines.Margin = new System.Windows.Forms.Padding(4);
            this.cbShowLines.Name = "cbShowLines";
            this.cbShowLines.Size = new System.Drawing.Size(97, 21);
            this.cbShowLines.TabIndex = 1;
            this.cbShowLines.Text = "Show lines";
            this.cbShowLines.UseVisualStyleBackColor = true;
            this.cbShowLines.CheckedChanged += new System.EventHandler(this.cbShowLines_CheckedChanged);
            // 
            // cbMultiselect
            // 
            this.cbMultiselect.AutoSize = true;
            this.cbMultiselect.Location = new System.Drawing.Point(12, 23);
            this.cbMultiselect.Margin = new System.Windows.Forms.Padding(4);
            this.cbMultiselect.Name = "cbMultiselect";
            this.cbMultiselect.Size = new System.Drawing.Size(96, 21);
            this.cbMultiselect.TabIndex = 0;
            this.cbMultiselect.Text = "Multiselect";
            this.cbMultiselect.UseVisualStyleBackColor = true;
            this.cbMultiselect.CheckedChanged += new System.EventHandler(this.cbMultiselect_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.bSetRootNodeCount);
            this.groupBox3.Controls.Add(this.tbRootNodeCount);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(8, 23);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(384, 127);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = " Initialization ";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(8, 53);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(368, 58);
            this.label7.TabIndex = 3;
            this.label7.Text = "VirtualTreeView should support huge numbers of nodes (like, millions) correctly. " +
    "Note though, that it\'ll consume equal amounts of memory.";
            // 
            // bSetRootNodeCount
            // 
            this.bSetRootNodeCount.Location = new System.Drawing.Point(305, 21);
            this.bSetRootNodeCount.Margin = new System.Windows.Forms.Padding(4);
            this.bSetRootNodeCount.Name = "bSetRootNodeCount";
            this.bSetRootNodeCount.Size = new System.Drawing.Size(71, 28);
            this.bSetRootNodeCount.TabIndex = 2;
            this.bSetRootNodeCount.Text = "Set";
            this.bSetRootNodeCount.UseVisualStyleBackColor = true;
            this.bSetRootNodeCount.Click += new System.EventHandler(this.bSetRootNodeCount_Click);
            // 
            // tbRootNodeCount
            // 
            this.tbRootNodeCount.Location = new System.Drawing.Point(132, 23);
            this.tbRootNodeCount.Margin = new System.Windows.Forms.Padding(4);
            this.tbRootNodeCount.Name = "tbRootNodeCount";
            this.tbRootNodeCount.Size = new System.Drawing.Size(164, 22);
            this.tbRootNodeCount.TabIndex = 1;
            this.tbRootNodeCount.Text = "10";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 27);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Root node count";
            // 
            // bAddChild
            // 
            this.bAddChild.Location = new System.Drawing.Point(202, 159);
            this.bAddChild.Name = "bAddChild";
            this.bAddChild.Size = new System.Drawing.Size(173, 28);
            this.bAddChild.TabIndex = 10;
            this.bAddChild.Text = "Add child";
            this.bAddChild.UseVisualStyleBackColor = true;
            this.bAddChild.Click += new System.EventHandler(this.bAddChild_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1153, 871);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmMain";
            this.Text = "SpkControls demo";
            this.tabControl1.ResumeLayout(false);
            this.tpVTV.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private KeylessTabControl tabControl1;
        private System.Windows.Forms.TabPage tpVTV;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button bSetRootNodeCount;
        private System.Windows.Forms.TextBox tbRootNodeCount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private Trees.VirtualTreeView vtvTree;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button bShowChosen;
        private System.Windows.Forms.ListBox lbHiddenNodes;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bHideSelected;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button bScrollToFocused;
        private System.Windows.Forms.Button bDeleteChildNodes;
        private System.Windows.Forms.Button bSetChildCount;
        private System.Windows.Forms.Button bDeleteNode;
        private System.Windows.Forms.Button bToggleSelected;
        private System.Windows.Forms.TextBox tbChildCount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox cbShowImages;
        private System.Windows.Forms.CheckBox cbShowExpandMarks;
        private System.Windows.Forms.CheckBox cbShowRootLines;
        private System.Windows.Forms.CheckBox cbShowLines;
        private System.Windows.Forms.CheckBox cbMultiselect;
        private System.Windows.Forms.Button bSetNodeHeight;
        private System.Windows.Forms.TextBox tbNodeHeight;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button bSetFontSize;
        private System.Windows.Forms.TextBox tbFontSize;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbDrawMode;
        private System.Windows.Forms.ImageList ilImages;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ListBox lbSelection;
        private KeylessTabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.CheckBox cbShowCheckboxes;
        private System.Windows.Forms.CheckBox cbAllowExpandCollapse;
        private System.Windows.Forms.CheckBox cbHotTrack;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ListBox lbCheckedNodes;
        private System.Windows.Forms.Button bInsertAfter;
        private System.Windows.Forms.Button bInsertBefore;
        private System.Windows.Forms.Button bAddChild;
    }
}

